#!/bin/sh

echo "What is your name?"
read username
useradd -mg wheel $username
passwd $username

pacman -Syyu
pacman -S xorg-server xorg-xinit git base-devel libxft libxinerama neovim dmenu \
ttf-linux-libertine ttf-font-awesome ttf-dejavu pipewire pipewire-pulse \
pulsemixer firefox sxhkd maim pass openssh xclip man task qemu-full newsboat \
yt-dlp mpv bluez bluez-utils go wget sxiv
git clone https://gitlab.com/kilbane/dwm.git /usr/local/src/dwm 
git clone https://gitlab.com/kilbane/slstatus.git /usr/local/src/slstatus
git clone https://gitlab.com/kilbane/st.git /usr/local/src/st
cd /usr/local/src/dwm
make clean install
cd ../slstatus
make clean install
cd ../st
make clean install
echo "%wheel ALL=(ALL:ALL) ALL" > /etc/sudoers.d/00_wheel_can_sudo
sudo -u $username firefox --headless
browserdir="/home/$username/.mozilla/firefox"
pdir="$browserdir/$(sed -n "/Default=.*.default-release/ s/.*=//p" "$browserdir/profiles.ini")"
overrides="$pdir/user-overrides.js"
userjs="$pdir/user.js"
updater="$pdir/updater.sh"
prefscleaner="$pdir/prefsCleaner.sh"
ln -fs "/home/$username/.config/firefox/preferences.js" "$overrides"
[ ! -f "$userjs" ] && curl -sL "https://raw.githubusercontent.com/arkenfox/user.js/master/user.js" > "$userjs"
[ ! -f "$updater" ] && curl -sL "https://raw.githubusercontent.com/arkenfox/user.js/master/updater.sh" > "$updater"
[ ! -f "$prefscleaner" ] && curl -sL "https://raw.githubusercontent.com/arkenfox/user.js/master/prefsCleaner.sh" > "$prefscleaner"
chown "$username:wheel" "$userjs" "$updater" "$prefscleaner"
chmod +x "$updater" "$prefscleaner"
#Find a better way to do this
cd /home/$username/
git clone https://gitlab.com/kilbane/update_arkenfox.git
git clone https://gitlab.com/kilbane/dotfiles.git
mv dotfiles/.* .
